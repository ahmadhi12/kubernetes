## Welcome Section

Minikube is local Kubernetes, focusing on making it easy to learn and develop for Kubernetes.

## Installation

```bash
  git clone https://gitlab.com/ahmadhi12/kubernetes.git
  cd kubernetes
  minikube start
  kubectl apply -f k8s
  minikube ip
```

## Documentation

[Documentation](https://minikube.sigs.k8s.io/docs/start/)

## Authors

- [@ahmadhi12](https://gitlab.com/ahmadhi12)
